Sleuthkit Apprentice
|
| 200 points
Tags: 

Author: LT 'syreal' Jones
Description

Download this disk image and find the flag. 
Note: if you are using the webshell, download and extract the disk image into 
/tmp not your home directory.

    Download compressed disk image

notes:
https://stefanoprenna.com/blog/2014/09/22/tutorial-how-to-mount-raw-images-img-images-on-linux/
#
suethkit:
mkdir tmp
mmls disk.flag.img
tsk_recover -o 360448 disk.flag.img tmp/


https://picoctf2021.haydenhousen.com/forensics/disk-disk-sleuth

