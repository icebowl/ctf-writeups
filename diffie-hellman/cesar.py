message = 'H98A9W_H6UM8W_6A_9_D6C_5ZCI9C8I_JBACIFAI' #encrypted message
LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'

for key in range(len(LETTERS)):
   translated = ''
   for symbol in message:
      if symbol in LETTERS:
         num = LETTERS.find(symbol)
         num = num - 5 
         if num < 0:
            num = num + len(LETTERS)
         translated = translated + LETTERS[num]
      else:
         translated = translated + symbol
print('Hacking key #%s: %s' % (key, translated))
