import collections


def get_list():
	# color list
	f = open("before.txt", 'r')
	contents = f.read()
	f.close()
	return contents

    
def main():
	the_list = get_list() # get the list of strings from the matrix file
	print(the_list)
	for n in range (0,len(the_list)):
		print(the_list[n]," ",end="")
	# using Counter to find frequency of elements
	frequency = collections.Counter(the_list)

	# printing the frequency
	print(dict(frequency))
	print(" -  - - - - \n")
	print (frequency)
	#pring key,item
	for key, value in frequency.items():
		 print(key, value)
  
if __name__ == "__main__":
  main()

